#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

if [ -n "$DISPLAY" ]; then
     BROWSER=chromium
fi

xstart

