########################
# housekeeping options #
########################

require 'subtle/subtlext'

# begin
#   #    require "#{ENV["HOME"]}/.config/subtle/launcher/launcher.rb"
#   require "/home/joschka/.config/subtle/launcher/launcher.rb"

#   # Set fonts
#    Subtle::Contrib::Launcher.fonts = [
#      "xft:DejaVu Sans Mono:pixelsize=12:antialias=true",
#      "xft:DejaVu Sans Mono:pixelsize=12:antialias=true" 
#    ]
# rescue LoadError => error
#   puts error
# end

# Window move/resize steps in pixel per keypress
set :step, 5

# Window screen border snapping
set :snap, 10

# Default starting gravity for windows. Comment out to use gravity of
# currently active client
set :gravity, :center

# Make transient windows urgent
set :urgent, false

# Honor resize size hints globally
set :resize, false

# Enable gravity tiling
set :tiling, false

#######################
# panel configuration #
#######################

# Set screen order
screen 1 do
  top [:soundcard, :mpd, :center, :views, :center, :spacer, :clock2, :battery]
  bottom [:title,:center, :wifi, :memory, :center, :spacer, :tray]
end

# default style configuration

# Color Definition
# 02243C Foreground
# F9F9F9 Background
# 005DB3 Busy
# 5BC236 Selected

# Style for all style elements
style :all do
  background  "#F9F9F9"
  icon        "#02243C"
  border      "#5BC236", 0
  padding     0, 3
#  font        "-*-*-*-*-*-*-14-*-*-*-*-*-*-*"
  #  font        "xft:montecarlo-9"
  font "xft:montecarlo:pixelsize=12"
end

# Style for the all views
style :views do
  foreground  "#02243C"
  backround "#F9F9F9"

  # Style for the active views
  style :focus do
    foreground  "#5BC236"
    border_bottom "#5BC236"
  end

  # Style for urgent window titles and views
  style :urgent do
    foreground  "#02243C"
  end

  # Style for occupied views (views with clients)
  style :occupied do
    foreground  "#005D83"
  end
end

# Style for sublets
style :sublets do
  foreground  "#02243C"
end

# Style for separator
style :separator do
  foreground  "#02243C"
  separator   "|"
end

# Style for focus window title
style :title do
  foreground  "#02243C"
end

# Style for active/inactive windows
style :clients do
  active    "#02243C", 0
  inactive  "#005DB3", 2
  margin    0
  width     50  
end

# Style for subtle
style :subtle do
  margin      0, 0, 0, 0
  panel       "#F9F9F9"
  background  "#F9F9F9"
  stipple     "#02243C"
end

# Style for sublet clock2
sublet :clock2 do
  date_format "%d-%m-%y"
end

##################
# gravity config #
##################

# Top left
gravity :top_left,       [   0,   0,  50,  33 ]
# gravity :top_left66,     [   0,   0,  50,  66 ]
gravity :top_left33,     [   0,   0,  50,  50 ]

# Top
gravity :top,            [   0,   0, 100,  50 ]
gravity :top66,          [   0,   0, 100,  66 ]
gravity :top33,          [   0,   0, 100,  34 ]

# Top right
gravity :top_right,      [  50,   0,  50,  50 ]
gravity :top_right66,    [  50,   0,  50,  66 ]
gravity :top_right33,    [  50,   0,  50,  33 ]

# Left
gravity :left,           [   0,   0,  50, 100 ]
gravity :left33,         [  0,  33,  50, 34 ] 
# gravity :left66,         [   0,   0,  66, 100 ]
gravity :left66,         [   0,   0,  33, 100 ]

# Center
gravity :center,         [   0,   0, 100, 100 ]
gravity :center66,       [  17,  17,  66,  66 ]
#gravity :center33,       [  33,  33,  33,  33 ]

# Right
gravity :right,          [  50,   0,  50, 100 ]
gravity :right66,        [  34,   0,  66, 100 ]
gravity :right33,        [  67,  50,  33, 100 ]

# Bottom left
gravity :bottom_left,    [   0,  67,  50,  33 ]
# gravity :bottom_left66,  [   0,  34,  50,  66 ]
gravity :bottom_left33,  [   0,  50,  50,  50 ]

# Bottom
gravity :bottom,         [   0,  50, 100,  50 ]
gravity :bottom66,       [   0,  34, 100,  66 ]
gravity :bottom33,       [   0,  67, 100,  33 ]

# Bottom right
gravity :bottom_right,   [  50,  50,  50,  50 ]
gravity :bottom_right66, [  50,  34,  50,  66 ]
gravity :bottom_right33, [  50,  67,  50,  33 ]

###############
# grab config #
###############

# Switch current view
grab "W-1", :ViewSwitch1
grab "W-2", :ViewSwitch2
grab "W-3", :ViewSwitch3
grab "W-4", :ViewSwitch4
grab "W-F1", :ViewSwitch5
grab "W-F2", :ViewSwitch6
#grab "W-F3", :ViewSwitch7
grab "W-5" do
  if Subtlext::Client.first("chat").nil?
    Subtlext::Subtle.spawn("urxvtc -name chat -e ssh-chat")
  end
  Subtlext::View[:chat].jump  
end

# grab "W-0" do
#   if Subtlext::Client.first("wicd-client").nil?
#     Subtlext::Subtle.spawn("wicd-client")
#   end
#   Subtlext::View[:network].jump  
# end

grab "W-9" do
  if Subtlext::Client.first("ncmpcpp").nil?
    Subtlext::Subtle.spawn("urxvtc -name ncmpcpp -e ncmpcpp")
  end
end

grab "W-C-r", :SubtleReload

# Toggle sticky mode of window (will be visible on all views)
grab "W-f", :WindowStick

# Select next windows
grab "W-j",  :WindowLeft
grab "W-m",  :WindowDown
grab "W-i",    :WindowUp
grab "W-k", :WindowRight

# Kill current window
grab "W-S-k", :WindowKill

# Cycle between gravitys
grab "W-q", [ :top_left, :top_left33] 
grab "W-w", [ :top,          :top66,          :top33          ]
grab "W-e", [ :top_right,    :top_right66,    :top_right33    ]
grab "W-a", [ :left,         :left66,         :left33         ]
grab "W-s", [ :center,       :center66] 
grab "W-d", [ :right,        :right66,        :right33        ]
grab "W-y", [ :bottom_left,  :bottom_left66,  :bottom_left33  ]
grab "W-x", [ :bottom,       :bottom66,       :bottom33       ]
grab "W-c", [ :bottom_right, :bottom_right66, :bottom_right33 ]

# Exec programs
grab "W-Return", 'urxvtc -e bash -c "tmux -q has-session && exec tmux attach-session -d || exec tmux new-session -n$USER -s$USER@$HOSTNAME"'
grab "W-v", "dmenu_run"
grab "W-BackSpace", "google-chrome"
# grab "W-9", "urxvtc -name ncmpcpp -e ncmpcpp"
# grab "W-0", "wicd-client"
grab "W-0", "mpd --kill; mpd"

# Lower window
grab "W-l", :WindowLower

# Raise window
grab "W-r", :WindowRaise

grab "W-B3", :WindowResize
  
#mpd bindings
grab "XF86AudioPlay", "ncmpcpp toggle"
grab "XF86AudioPause", "ncmpcpp toggle" 
grab "XF86AudioNext", "ncmpcpp next" 
grab "XF86AudioPrev", "ncmpcpp prev"

# Screen resolutian config
grab "W-p", "resolution"

# Backlight control
grab "XF86MonBrightnessDown", "xbacklight -dec 10 -time 0 -steps 0"
grab "XF86MonBrightnessUp", "xbacklight -inc 10 -time 0 -steps 0"

# Subtle launcher
# grab "W-v" do
#    Subtle::Contrib::Launcher.run
# end

# Toggle sticky mode
grab "W-g", :WindowStick

# Switch Views with Arrow Keys
grab "W-period", :ViewNext
grab "W-comma", :ViewPrev

grab "W-b", "subtler -cT chat web"

#####################
# tag configuration #
#####################

tag "term", "urxvtc"
tag "web", "chromium|flash|firefox|luakit|google-chrome"
tag "dev", "emacs|eclipse"
tag "lit", "evince"
tag "media", "ncmpcpp|mplayer|spotify|mixxx|ncmpc|vlc"
tag "chat", "chat|pidgin|empathy"
tag "rdesktop","rdesktop"
tag "vm", "VirtualBox|vmware|emulator-arm"

tag "flash" do
   match "<unknown>|plugin-container|exe|operapluginwrapper|npviewer.bin" 
   stick true
end

######################
# view configuration #
######################

view "term", "terms|default"
view "web", "web"
view "dev", "dev"
view "reader", "lit"
view "rdp", "rdesktop"
view "media", "media"
view "vm", "vm"
view "chat", "chat"

