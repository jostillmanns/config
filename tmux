unbind C-b
set -g prefix C-q

bind-key h split-window -h
bind-key v split-window -v

bind j select-pane -L
bind m select-pane -D 
bind i select-pane -U
bind k select-pane -R

set-window-option -g automatic-rename off

bind-key -n M-j next
bind-key -n M-k prev