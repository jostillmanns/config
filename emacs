(add-to-list 'load-path "~/.emacs.d/plugins/")

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector ["#242424" "#e5786d" "#95e454" "#cae682" "#8ac6f2" "#333366" "#ccaa8f" "#f6f3e8"])
 '(custom-enabled-themes nil)
 '(display-time-mode nil)
 '(ido-mode (quote both) nil (ido))
 '(menu-bar-mode nil)
 '(scroll-bar-mode nil)
 '(tool-bar-mode nil)
 '(tooltip-mode nil)
 '(uniquify-buffer-name-style (quote post-forward) nil (uniquify)))

;; (when (fboundp 'windmove-default-keybindings)
;;   (windmove-default-keybindings))

(global-set-key (kbd "C-c j")  'windmove-left)
(global-set-key (kbd "C-c k") 'windmove-right)
(global-set-key (kbd "C-c i")    'windmove-up)
(global-set-key (kbd "C-c m")  'windmove-down)

;; Line Numbers
(require 'linum)
(global-linum-mode 1)

;; Auctex
(load "auctex.el" nil t t)
(load "preview-latex.el" nil t t)
(load "csharp-mode.el" nil t t)

;; Load csharp-mode on loading cs file
  (autoload 'csharp-mode "csharp-mode" "Major mode for editing C# code." t)
  (setq auto-mode-alist
     (append '(("\\.cs$" . csharp-mode)) auto-mode-alist))

;; Ruby Mode comment region with C-c #
(add-hook 'ruby-mode-hook
	  (lambda ()
	    (define-key ruby-mode-map "\C-c#" 'comment-or-uncomment-region)
	    )
	  )

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(set-face-attribute 'default nil :height 100)


;; NASM Mode
(autoload 'nasm-mode "~/.emacs.d/plugins/nasm-mode.el" "" t)
(add-to-list 'auto-mode-alist '("\\.\\(asm\\|s\\)$" . nasm-mode))

(setq initial-major-mode 'org-mode)
(setq initial-scratch-message "")


;; ;; Android-Mode
;; (add-to-list 'load-path "/home/joschka/.emacs.d/plugins/android-mode")
;; (require 'android-mode)
;; (require 'android)
;; (setq android-mode-sdk-dir "/opt/android-sdk")
;; (add-hook 'gud-mode-hook
;; 	  (lambda ()
;;             (add-to-list 'gud-jdb-classpath "/opt/android-sdk/platforms/android-10/android.jar")
;;             ))

;; CSV Mode
(load "csv-mode.el" nil t t)

;; Moves emacs backups

(setq
 backup-by-copying t                    ; don't clobber symlinks
 backup-directory-alist
 '(("." . "~/.saves"))                  ; don't litter my fs tree
 delete-old-versions t
 kept-new-versions 6
 kept-old-versions 2
 version-control t)                     ; use versioned backups

;; Move auto-save-files

(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))


;; Date Function
(require 'calendar )
(defun insdate-insert-current-date (&optional omit-day-of-week-p)
    "Insert today's date using the current locale.
  With a prefix argument, the date is inserted without the day of
  the week."
    (interactive "P*")
    (insert (calendar-date-string (calendar-current-date) nil
				  omit-day-of-week-p)))

(global-set-key "\C-x\M-d" `insdate-insert-current-date)

;; unique document naming
(require 'uniquify)

;; flyspell
(require 'flyspell)
(global-set-key (kbd "<f8>")   'fd-switch-dictionary)


;; Make all “yes or no” prompts show “y or n” instead

(fset 'yes-or-no-p 'y-or-n-p) 
(put 'downcase-region 'disabled nil)
